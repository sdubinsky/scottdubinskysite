# config valid for current version and patch releases of Capistrano

set :application, 'sdubinskysite'
set :repo_url, 'git@gitlab.com:sdubinsky/scottdubinskysite'
set :deploy_to, '/sdubinskysite'
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, 'master'
set :default_env, {
      'ASDF_DATA_DIR': '/opt/asdf',
      'BUNDLE_PATH': '/sdubinskysite/.bundle',
      'BUNDLE_DEPLOYMENT': 'TRUE'
    }
desc 'migrations'

append :linked_files, 'config.yml', 'session_secret.txt'
# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "tmp/webpacker", "public/system", "vendor", "storage"
append :linked_dirs, 'log'
# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

namespace :systemd do
  desc 'restart server'
  task :restart do
    on roles(:web) do
      sudo :systemctl, 'restart', 'sdubinsky'
    end
  end
end

desc 'bundle'
task :bundle do
  on roles(:web) do
    within release_path do
      execute 'asdf', 'exec', 'bundle'
    end
  end
end

desc 'migrations'
task 'db:migrate'.to_sym do
  on roles(:web) do
    within release_path do
      execute 'asdf', 'exec', 'bundle', 'exec', 'rake', 'db:migrate'
    end
  end
end
after 'deploy', 'bundle'
after 'deploy', 'db:migrate'
after 'deploy', 'systemd:restart'
